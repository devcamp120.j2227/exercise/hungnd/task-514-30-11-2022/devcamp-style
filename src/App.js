import avatar from './assests/images/avatar.jpg';

import "./App.css";

function App() {
  return (
    <div className='devcamp'>
      <div className='devcamp-container'>
        <div>
          <img className='devcamp-avatar' src={avatar} alt="avatar User" ></img>
        </div>
        <div className='devcamp-quote'>
          <p><i>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</i></p>
        </div>
        <div>
          <span className='devcamp-name'>
            <b>
              Tammy Stevens
            </b>
          </span>
          <span className='devcamp-content'>&nbsp; * &nbsp;Front End Developer</span>
        </div>
      </div>
    </div>
  );
}

export default App;
